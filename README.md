[Replications][project]
===

_:mag: Replicating various works._


Purpose
---
To improve my understanding of interesting works through replication.
By replicating existing works (or failing to) this project also helps to improve the scientific process.


Catalog
---
### 12020
- [ ] de Vries, Joey. [Learn OpenGL](https://learnopengl.com/). Learn OpenGL.

### 12013
- [ ] Maalouf, Ibrahim. [True Sorry](https://www.youtube.com/watch?v=HXzv7P7qGdM). Illusions.

### 12012
- [ ] Patel, Amit. [2D Visibility](https://www.redblobgames.com/articles/visibility/). Red Blob Games.


Backlog
---
- Brockmann, Dirk. [Flock'n Roll](http://www.complexity-explorables.org/explorables/flockn-roll/). Complexity Explorables.
- Brockmann, Dirk. [I Herd You!](http://www.complexity-explorables.org/explorables/i-herd-you/). Complexity Explorables.
- Lopes, Cristina. [Exercises in Programming Style](https://github.com/crista/exercises-in-programming-style). CRC Press.
- Schloss, James. [Introduction to Computational Thinking: Ray Tracing](https://computationalthinking.mit.edu/). MIT 18.S191.
- Shiffman, Daniel. [2D Black Hole Visualization](https://thecodingtrain.com/CodingChallenges/144-black-hole-visualization). The Coding Train.
- Shirley, Peter. [Ray Tracing in One Weekend Series](https://raytracing.github.io/). Ray Tracing in One Weekend Series.


Inspiration
---
- Tallec, Corentin et al. [Reproducing "World Models"](https://ctallec.github.io/world-models/). ctallec.github.io.


License
---
I have licensed this project under [CC BY-NC 4.0][license].

You may negociate a waiver for the non-commercial restriction.
Please contact me at legal@projectaxil.us to do so.


[project]: https://github.com/mraxilus/replications
[license]: https://creativecommons.org/licenses/by-nc/4.0/
