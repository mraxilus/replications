{.experimental: "codeReordering".}

import nimgl/[glfw, opengl]


if isMainModule:
    main()


proc main() =
    # Initialise GLFW with OpenGL 3.3's core profile.
    assert glfwInit()  
    glfwWindowHint(GLFWContextVersionMajor, 3)
    glfwWindowHint(GLFWContextVersionMinor, 3)
    glfwWindowHint(GLFWOpenglForwardCompat, GLFW_TRUE)
    glfwWindowHint(GLFWOpenglProfile, GLFW_OPENGL_CORE_PROFILE)

    # Create window.
    let window: GLFWWindow = glfwCreateWindow(800, 600, "LearnOpenGL")
    if window == nil:
        echo "Failed to create GLFW window."
        glfwTerminate()
        quit(-1)

    # Make windows context main OpenGL context.
    window.makeContextCurrent()

    # Setup callbacks for inputs and window resizing.
    discard window.setKeyCallback(processKeyCallback)
    discard window.setFramebufferSizeCallback(processFramebufferSizeCallback)

    # Initialize OpenGL.
    assert glInit()

    # Perform render loop until closed.
    while not window.windowShouldClose:
        window.swapBuffers()
        glfwPollEvents()

    # Terminate GLFW.
    window.destroyWindow()
    glfwTerminate()


proc processKeyCallback(window: GLFWWindow, key: int32, scancode: int32,
        action: int32, mods: int32): void {.cdecl.} =
    ## Monintor for escape key and close window if pressed.
    if key == cast[int](GLFWKey.Escape) and action == GLFWPress:
        window.setWindowShouldClose(true)


proc processFramebufferSizeCallback(window: GLFWWindow, width: int32,
        height: int32): void {.cdecl.} =
    ## Inform OpenGL of current window size and adjust viewport accordingly.
    glViewport(0, 0, width, height)

