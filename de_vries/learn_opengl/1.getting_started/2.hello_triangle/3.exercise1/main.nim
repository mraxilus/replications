{.experimental: "codeReordering".}

import nimgl/[glfw, opengl]


if isMainModule:
    main()


proc main() =
    # Initialise GLFW with OpenGL 3.3's core profile.
    assert glfwInit()
    glfwWindowHint(GLFWContextVersionMajor, 3)
    glfwWindowHint(GLFWContextVersionMinor, 3)
    glfwWindowHint(GLFWOpenglForwardCompat, GLFW_TRUE)
    glfwWindowHint(GLFWOpenglProfile, GLFW_OPENGL_CORE_PROFILE)

    # Create window.
    let window: GLFWWindow = glfwCreateWindow(800, 600, "LearnOpenGL")
    if window == nil:
        echo "Failed to create GLFW window."
        glfwTerminate()
        quit(-1)

    # Make windows context main OpenGL context.
    window.makeContextCurrent()

    # Setup callbacks for inputs and window resizing.
    discard window.setKeyCallback(processKeyCallback)
    discard window.setFramebufferSizeCallback(processFramebufferSizeCallback)

    # Initialize OpenGL.
    assert glInit()

    # Define triangle objects, definitions, and shaders.
    var
        vertex_array_object: uint32
        vertex_buffer_object: uint32
        vertices = @[
          # First triangle.
            -0.8f, -0.5f, 0.0f,
            -0.4f, 0.5f, 0.0f,
             0.0f, -0.5f, 0.0f,

            # Second triangle.
                0.0f, -0.5f, 0.0f,
                0.4f, 0.5f, 0.0f,
                0.8f, -0.5f, 0.0f,
        ]
        vertex_shader = glCreateShader(GL_VERTEX_SHADER)
        vertex_shader_source: cstring = """
            #version 330 core
            layout (location = 0) in vec3 aPos;

            void main() {
                gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);
            }
        """
        fragment_shader = glCreateShader(GL_FRAGMENT_SHADER)
        fragment_shader_source: cstring = """
            #version 330 core
            out vec4 FragColor;

            void main() {
                FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);
            }
        """
        shader_program = glCreateProgram()

    # Compile and verify vertex shader.
    glShaderSource(vertex_shader, 1, vertex_shader_source.addr, nil)
    glCompileShader(vertex_shader)
    checkShaderStatus(vertex_shader)

    # Compile and verify fragment shader.
    glShaderSource(fragment_shader, 1, fragment_shader_source.addr, nil)
    glCompileShader(fragment_shader)
    checkShaderStatus(fragment_shader)

    # Link shaders together into program.
    glAttachShader(shader_program, vertex_shader)
    glAttachShader(shader_program, fragment_shader)
    glLinkProgram(shader_program)
    checkProgramStatus(shader_program)

    # Cleanup shaders.
    glDeleteShader(vertex_shader)
    glDeleteShader(fragment_shader)

    # Generate and bind vertex array object.
    glGenVertexArrays(1, vertex_array_object.addr)
    glBindVertexArray(vertex_array_object)

    # Generate and bind vertex buffer object.
    glGenBuffers(1, vertex_buffer_object.addr)
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object)
    glBufferData(GL_ARRAY_BUFFER, sizeof(float32) * vertices.len,
                 vertices[0].addr, GL_STATIC_DRAW)

    # Inform OpenGL of vertex data format.
    glVertexAttribPointer(0'u32, 3, EGL_FLOAT, false, 3 * sizeof(float32), nil)
    glEnableVertexAttribArray(0)

    # Unbind vertex buffer and array objects.
    glBindBuffer(GL_ARRAY_BUFFER, 0)
    glBindVertexArray(0)

    # Perform render loop until closed.
    while not window.windowShouldClose:
        # Clear frame buffer.
        glClearColor(0.2f, 0.2f, 0.2f, 1f)
        glClear(GL_COLOR_BUFFER_BIT)

        # Draw triangles.
        glUseProgram(shader_program)
        glBindVertexArray(vertex_array_object)
        glDrawArrays(GL_TRIANGLES, 0, 6)
        window.swapBuffers()

        # Process inputs.
        glfwPollEvents()

    # Cleanup GLFW.
    window.destroyWindow()
    glfwTerminate()


proc processKeyCallback(window: GLFWWindow, key: int32, scancode: int32,
                        action: int32, mods: int32): void {.cdecl.} =
    ## Monintor for escape key and close window if pressed.
    if key == GLFWKey.Escape and action == GLFWPress:
        window.setWindowShouldClose(true)


proc processFramebufferSizeCallback(window: GLFWWindow, width: int32,
                                    height: int32): void {.cdecl.} =
    ## Inform OpenGL of current window size and adjust viewport accordingly.
    glViewport(0, 0, width, height)


proc checkShaderStatus(shader: uint32) =
    ## Verify if shader compiled correctly.
    var status: int32
    glGetShaderiv(shader, GL_COMPILE_STATUS, status.addr)
    if status != ord(GL_TRUE):
        var
            log_length: int32
            message = newSeq[char](1024)
        glGetShaderInfoLog(shader, int32(len(message)), log_length.addr,
                message[0].addr)
        echo message


proc checkProgramStatus(program: uint32) =
    ## Verify if program linked correctly.
    var status: int32
    glGetProgramiv(program, GL_LINK_STATUS, status.addr)
    if status != ord(GL_TRUE):
        var
            log_length: int32
            message = newSeq[char](1024)
        glGetProgramInfoLog(program, int32(len(message)), log_length.addr,
                            message[0].addr)
        echo message

