{.experimental: "codeReordering".}

import nimgl/[glfw, opengl]
import math


if isMainModule:
    main()


proc main() =
    # Initialise GLFW with OpenGL 3.3's core profile.
    assert glfwInit()
    glfwWindowHint(GLFWContextVersionMajor, 3)
    glfwWindowHint(GLFWContextVersionMinor, 3)
    glfwWindowHint(GLFWOpenglForwardCompat, GLFW_TRUE)
    glfwWindowHint(GLFWOpenglProfile, GLFW_OPENGL_CORE_PROFILE)

    # Create window.
    let window: GLFWWindow = glfwCreateWindow(800, 600, "LearnOpenGL")
    if window == nil:
        echo "Failed to create GLFW window."
        glfwTerminate()
        quit(-1)

    # Make windows context main OpenGL context.
    window.makeContextCurrent()

    # Setup callbacks for inputs and window resizing.
    discard window.setKeyCallback(processKeyCallback)
    discard window.setFramebufferSizeCallback(processFramebufferSizeCallback)

    # Initialize OpenGL.
    assert glInit()

    # Define triangle objects, definitions, and shaders.
    var
        mesh_count: int32 = 1
        vertex_array_objects = newSeq[uint32](mesh_count)
        vertex_buffer_objects = newSeq[uint32](mesh_count)
        vertices = @[@[
            # Triangle.
                # Positions (3)  Colors (3)
            0.5f, -0.5f, 0f, 1f, 0f, 0f, # Bottom right.
            -0.5f, -0.5f, 0f, 0f, 1f, 0f, # Bottom left.
            0f, 0.5f, 0f, 0f, 0f, 1f, # Top.
        ]]
        vertex_shaders = newSeq[uint32](mesh_count)
        vertex_shader_sources: seq[cstring] = @[
            cstring("""
                #version 330 core

                layout (location = 0) in vec3 aPos;
                layout (location = 1) in vec3 aColor;

                out vec3 vertexColor;

                void main() {
                    gl_Position = vec4(aPos, 1.0);
                    vertexColor = aColor;
                }
            """)
        ]
        fragment_shaders = newSeq[uint32](mesh_count)
        fragment_shader_sources: seq[cstring] = @[
            cstring("""
                #version 330 core

                in vec3 vertexColor;

                out vec4 FragColor;

                void main() {
                    FragColor = vec4(vertexColor, 1.0);
                }
            """),
        ]
        shader_programs = newSeq[uint32](mesh_count)

    # Compile vertex shaders.
    for i, _ in vertex_shaders:
        vertex_shaders[i] = glCreateShader(GL_VERTEX_SHADER)
        glShaderSource(vertex_shaders[i], 1, vertex_shader_sources[i].addr, nil)
        glCompileShader(vertex_shaders[i])
        checkShaderStatus(vertex_shaders[i])

    # Compile fragment shaders.
    for i, _ in fragment_shaders:
        fragment_shaders[i] = glCreateShader(GL_FRAGMENT_SHADER)
        glShaderSource(fragment_shaders[i], 1, fragment_shader_sources[i].addr, nil)
        glCompileShader(fragment_shaders[i])
        checkShaderStatus(fragment_shaders[i])

    # Link shaders into programs.
    for i, _ in shader_programs:
        shader_programs[i] = glCreateProgram()
        glAttachShader(shader_programs[i], vertex_shaders[i])
        glAttachShader(shader_programs[i], fragment_shaders[i])
        glLinkProgram(shader_programs[i])
        checkProgramStatus(shader_programs[i])

    # Cleanup shaders.
    for i, _ in shader_programs:
        glDeleteShader(vertex_shaders[i])
        glDeleteShader(fragment_shaders[i])

    # Configure triangles' OpenGL object data.
    for i, _ in vertex_array_objects:
        # Generate and bind vertex array object.
        glGenVertexArrays(1, vertex_array_objects[i].addr)
        glBindVertexArray(vertex_array_objects[i])

        # Generate and bind vertex buffer object.
        glGenBuffers(1, vertex_buffer_objects[i].addr)
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_objects[i])
        glBufferData(GL_ARRAY_BUFFER, sizeof(float32) * vertices[i].len,
                     vertices[i][0].addr, GL_STATIC_DRAW)

        # Inform OpenGL of vertex position data format.
        glVertexAttribPointer(0'u32, 3, EGL_FLOAT, false, 6 * sizeof(float32), nil)
        glEnableVertexAttribArray(0)

        # Inform OpenGL of vertex color data format.
        glVertexAttribPointer(1'u32, 3, EGL_FLOAT, false, 6 * sizeof(float32),
                cast[ptr uint32](3 * sizeof(float32)))
        glEnableVertexAttribArray(1)

        # Unbind vertex buffer and array objects.
        glBindBuffer(GL_ARRAY_BUFFER, 0)
        glBindVertexArray(0)

    # Perform render loop until closed.
    while not window.windowShouldClose:
        # Clear frame buffer.
        glClearColor(0.2f, 0.2f, 0.2f, 1f)
        glClear(GL_COLOR_BUFFER_BIT)

        # Draw triangles.
        for i, _ in vertex_array_objects:
            glUseProgram(shader_programs[i])
            glBindVertexArray(vertex_array_objects[i])
            glDrawArrays(GL_TRIANGLES, 0, 3)

        window.swapBuffers()

        # Process inputs.
        glfwPollEvents()

    # Cleanup OpenGL objects and programs.
    glDeleteBuffers(mesh_count, vertex_buffer_objects[0].addr)
    glDeleteVertexArrays(mesh_count, vertex_array_objects[0].addr)
    for i, _ in shader_programs:
        glDeleteProgram(shader_programs[i])

    # Cleanup GLFW.
    window.destroyWindow()
    glfwTerminate()


proc processKeyCallback(window: GLFWWindow, key: int32, scancode: int32,
                        action: int32, mods: int32): void {.cdecl.} =
    ## Monintor for escape key and close window if pressed.
    if key == GLFWKey.Escape and action == GLFWPress:
        window.setWindowShouldClose(true)


proc processFramebufferSizeCallback(window: GLFWWindow, width: int32,
                                    height: int32): void {.cdecl.} =
    ## Inform OpenGL of current window size and adjust viewport accordingly.
    glViewport(0, 0, width, height)


proc checkShaderStatus(shader: uint32) =
    ## Verify if shader compiled correctly.
    var status: int32
    glGetShaderiv(shader, GL_COMPILE_STATUS, status.addr)
    if status != ord(GL_TRUE):
        var
            log_length: int32
            message = newSeq[char](1024)
        glGetShaderInfoLog(shader, int32(len(message)), log_length.addr,
                message[0].addr)
        echo message


proc checkProgramStatus(program: uint32) =
    ## Verify if program linked correctly.
    var status: int32
    glGetProgramiv(program, GL_LINK_STATUS, status.addr)
    if status != ord(GL_TRUE):
        var
            log_length: int32
            message = newSeq[char](1024)
        glGetProgramInfoLog(program, int32(len(message)), log_length.addr,
                            message[0].addr)
        echo message

