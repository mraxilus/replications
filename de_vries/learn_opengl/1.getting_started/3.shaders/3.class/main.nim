{.experimental: "codeReordering".}

import nimgl/[glfw, opengl]
import shader


if isMainModule:
    main()


proc main() =
    # Initialise GLFW with OpenGL 3.3's core profile.
    assert glfwInit()
    glfwWindowHint(GLFWContextVersionMajor, 3)
    glfwWindowHint(GLFWContextVersionMinor, 3)
    glfwWindowHint(GLFWOpenglForwardCompat, GLFW_TRUE)
    glfwWindowHint(GLFWOpenglProfile, GLFW_OPENGL_CORE_PROFILE)

    # Create window.
    let window: GLFWWindow = glfwCreateWindow(800, 600, "LearnOpenGL")
    if window == nil:
        echo "Failed to create GLFW window."
        glfwTerminate()
        quit(-1)

    # Make windows context main OpenGL context.
    window.makeContextCurrent()

    # Setup callbacks for inputs and window resizing.
    discard window.setKeyCallback(processKeyCallback)
    discard window.setFramebufferSizeCallback(processFramebufferSizeCallback)

    # Initialize OpenGL.
    assert glInit()

    # Define triangle objects, definitions, and shaders.
    var
        mesh_count: int32 = 1
        vertex_array_objects = newSeq[uint32](mesh_count)
        vertex_buffer_objects = newSeq[uint32](mesh_count)
        vertices = @[@[
            # Triangle.
            # Positions (3),  Colors (3)
            0.5f, -0.5f, 0f, 1f, 0f, 0f, # Bottom right.
            -0.5f, -0.5f, 0f, 0f, 1f, 0f, # Bottom left.
            0f, 0.5f, 0f, 0f, 0f, 1f, # Top.
        ]]
        shaders = @[
            initShaderProgram(
                (GL_VERTEX_SHADER, "shader.vert"),
                (GL_FRAGMENT_SHADER, "shader.frag"),
            )
        ]


    # Configure triangles' OpenGL object data.
    for i, _ in vertex_array_objects:
        # Generate and bind vertex array object.
        glGenVertexArrays(1, vertex_array_objects[i].addr)
        glBindVertexArray(vertex_array_objects[i])

        # Generate and bind vertex buffer object.
        glGenBuffers(1, vertex_buffer_objects[i].addr)
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_objects[i])
        glBufferData(GL_ARRAY_BUFFER, sizeof(float32) * vertices[i].len,
                     vertices[i][0].addr, GL_STATIC_DRAW)

        # Inform OpenGL of vertex position data format.
        glVertexAttribPointer(0'u32, 3, EGL_FLOAT, false, 6 * sizeof(float32), nil)
        glEnableVertexAttribArray(0)

        # Inform OpenGL of vertex color data format.
        glVertexAttribPointer(1'u32, 3, EGL_FLOAT, false, 6 * sizeof(float32),
                cast[ptr uint32](3 * sizeof(float32)))
        glEnableVertexAttribArray(1)

        # Unbind vertex buffer and array objects.
        glBindBuffer(GL_ARRAY_BUFFER, 0)
        glBindVertexArray(0)

    # Perform render loop until closed.
    while not window.windowShouldClose:
        # Clear frame buffer.
        glClearColor(0.2f, 0.2f, 0.2f, 1f)
        glClear(GL_COLOR_BUFFER_BIT)

        # Draw triangles.
        for i, _ in vertex_array_objects:
            shaders[i].use()
            glBindVertexArray(vertex_array_objects[i])
            glDrawArrays(GL_TRIANGLES, 0, 3)

        window.swapBuffers()

        # Process inputs.
        glfwPollEvents()

    # Cleanup OpenGL objects and programs.
    glDeleteBuffers(mesh_count, vertex_buffer_objects[0].addr)
    glDeleteVertexArrays(mesh_count, vertex_array_objects[0].addr)
    for shader in shaders:
        shader.delete()

    # Cleanup GLFW.
    window.destroyWindow()
    glfwTerminate()


proc processKeyCallback(window: GLFWWindow, key: int32, scancode: int32,
                        action: int32, mods: int32): void {.cdecl.} =
    ## Monintor for escape key and close window if pressed.
    if key == GLFWKey.Escape and action == GLFWPress:
        window.setWindowShouldClose(true)


proc processFramebufferSizeCallback(window: GLFWWindow, width: int32,
                                    height: int32): void {.cdecl.} =
    ## Inform OpenGL of current window size and adjust viewport accordingly.
    glViewport(0, 0, width, height)

