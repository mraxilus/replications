import nimgl/[glfw, opengl]
import streams

type
    ShaderProgram* = object
        id*: uint32


proc check_shader_status(id: uint32) =
    ## Verify if shader compiled correctly.
    var status: int32
    glGetShaderiv(id, GL_COMPILE_STATUS, status.addr)
    if status != ord(GL_TRUE):
        var
            log_length: int32
            message = newSeq[char](1024)
        glGetShaderInfoLog(id, int32(len(message)), log_length.addr,
                message[0].addr)
        echo cast[string](message)


proc check_status(shader: ShaderProgram) =
    ## Verify if program linked correctly.
    var status: int32
    glGetProgramiv(shader.id, GL_LINK_STATUS, status.addr)
    if status != ord(GL_TRUE):
        var
            log_length: int32
            message = newSeq[char](1024)
        glGetProgramInfoLog(shader.id, int32(len(message)), log_length.addr,
                            message[0].addr)
        echo cast[string](message)
        # TODO: Make program status function return appropriate values.


proc init_shader_program*(shaders: varargs[tuple[`type`: GLenum, path: string]]): ShaderProgram =
    var
        ids = newSeq[uint32](shaders.len)
        sources = newSeq[cstring](shaders.len)

    for i, shader in shaders:
        # Open shader source for reading.
        var stream = newFileStream(shader.path, fmRead)
        if isNil(stream):
            raise newException(OSError, "Unable to open shader file:" & shader.path)

        # Read shader source.
        var contents, line: string
        while stream.readLine(line):
            contents &= line & "\n"
        sources[i] = contents
        stream.close()

        # Compile shader.
        ids[i] = glCreateShader(shader.`type`)
        glShaderSource(ids[i], 1, sources[i].addr, nil)
        glCompileShader(ids[i])
        checkShaderStatus(ids[i])

    # Link shaders into program.
    result.id = glCreateProgram()
    for id in ids:
        glAttachShader(result.id, id)
    glLinkProgram(result.id)
    result.check_status()

    # Cleanup shaders.
    for i, _ in ids:
        glDeleteShader(ids[i])


proc set_uniform*[T](shader: ShaderProgram, name: cstring, value: T) =
    ## Set value of GLSL uniform defined in shader.
    var location = glGetUniformLocation(shader.id, name)
    case T
    of bool:
        glUniform1i(shader.id, location, value.int32)
    of int:
        glUniform1i(shader.id, location, value.int32)
    of float:
        glUniform1f(shader.id, location, value.float32)
    else:
        raise newException(TypeError, "Failed to set uniform; invalid type.")


proc use*(shader: ShaderProgram) =
    ## Set shader as OpenGL's active program.
    glUseProgram(shader.id)


proc delete*(shader: ShaderProgram) =
    ## Delete shader program from OpenGL.
    glDeleteProgram(shader.id)

