#version 330 core

in vec3 vertexPosition;

out vec4 FragColor;

void main() {
    // Negative values will be clipped to 0.
    // This means that bottom value should be black.
    // I.e. (-.5, -.5) -> (0, 0) -> Black.
    //      (0, .5) -> (0, 0.5) -> Half brightness green.
    //      (.5, -.5) -> (.5, 0) -> Half brightness red.
    FragColor = vec4(vertexPosition, 1.0);
}
