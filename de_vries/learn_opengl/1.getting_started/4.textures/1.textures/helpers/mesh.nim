{.experimental: "code_reordering".}

import nimgl/opengl


type
    Mesh* = ref object
        vertex_array_object: uint32
        vertex_buffer_object: uint32
        element_buffer_object: uint32
        vertices*: seq[float32]
        indices*: seq[uint32]
        attributes*: seq[Attribute]
    Attribute* = object
        size*: int32
        count*: int32


proc init_attribute(size: int32, count: int32): Attribute =
    # Inititalise attribute settings.
    result = Attribute(size: size, count: count);


proc init_mesh*(vertices: seq[float32], indices: seq[uint32],
                attributes: seq[tuple[size, count: int32]]): Mesh =
    ## Inititalise mesh from raw data.

    # Generate attributes.
    var attributes_init = new_seq[Attribute]()
    for a in attributes:
        attributes_init.add(init_attribute(a.size, a.count))

    result = Mesh(
        vertices: vertices,
        indices: indices,
        attributes: attributes_init,
    )

    # Generate and bind vertex array object.
    gl_gen_vertex_arrays(1, result.vertex_array_object.addr)
    gl_bind_vertex_array(result.vertex_array_object)

    # Generate and bind vertex buffer object.
    gl_gen_buffers(1, result.vertex_buffer_object.addr)
    gl_bind_buffer(GL_ARRAY_BUFFER, result.vertex_buffer_object)
    gl_buffer_data(GL_ARRAY_BUFFER, sizeof(float32) * result.vertices.len,
                   result.vertices[0].addr, GL_STATIC_DRAW)

    # Setup element buffer object from indicies.
    gl_gen_buffers(1, result.element_buffer_object.addr)
    gl_bind_buffer(GL_ELEMENT_ARRAY_BUFFER, result.element_buffer_object)
    gl_buffer_data(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32) * result.indices.len,
                   result.indices[0].addr, GL_STATIC_DRAW)

    # Determine stride for vertex data.
    var stride = 0
    for attribute in result.attributes:
        stride += attribute.size
    stride *= sizeof(float32)
        
    # Inform OpenGL of vertex data format.
    var offset = 0
    for i, attribute in result.attributes:
        gl_vertex_attrib_pointer(
            i.uint32, 
            attribute.size, 
            EGL_FLOAT, 
            false, 
            stride.int32, 
            cast[ptr uint32](offset)
        )
        gl_enable_vertex_attrib_array(i.uint32)

        offset += attribute.size * sizeof(float32)


proc attach*(mesh: Mesh) =
    # Attach all associated data to OpenGL context. 
    gl_bind_vertex_array(mesh.vertex_array_object)


proc delete*(mesh: Mesh) =
    ## Delete all associated data from OpenGL context.
    gl_delete_buffers(1, mesh.vertex_buffer_object.addr)
    gl_delete_vertex_arrays(1, mesh.vertex_array_object.addr)


proc detach*(mesh: Mesh) =
    ## Unbind all associated data from OpenGL context.
    gl_bind_buffer(GL_ARRAY_BUFFER, 0)
    gl_bind_vertex_array(0)

