{.experimental: "code_reordering".}

import nimgl/opengl
import mesh
import shader
import texture

type
    Model = object
        mesh*: Mesh
        shader*: Shader
        texture*: Texture


proc init_model*(mesh: Mesh, shader: Shader, texture: Texture): Model =
    ## Initialise model from encapsulation of a mesh, shader, and texture object.
    result = Model(
        mesh: mesh,
        shader: shader,
        texture: texture,
    )


proc init_model*(vertices: seq[float32], indices: seq[uint32],
                 attributes: seq[tuple[size, count: int32]],
                 shaders: seq[tuple[`type`: GLenum, path: string]],
                 texture: string): Model =
    ## Initialise model from raw data.

    result = init_model(
        mesh = init_mesh(
            vertices = vertices,
            indices = indices,
            attributes = attributes,
        ),
        shader = init_shader(
            shaders = shaders,
        ),
        texture = init_texture(path = texture),
    )


proc attach*(model: Model) =
    ## Bind/use all associated data to current OpenGL context.
    model.mesh.attach()
    model.shader.attach()
    model.texture.attach()


proc detach*(model: Model) =
    ## Unbind all associated data to current OpenGL context.
    model.mesh.detach()


proc delete*(model: Model) =
    ## Delete all associated data in current OpenGL context
    model.mesh.delete()
    model.shader.delete()


proc draw*(model: Model) =
    ## Drag triangles associated with model.
    model.attach()
    gl_draw_elements(GL_TRIANGLES, model.mesh.vertices.len.int32, GL_UNSIGNED_INT, nil)

