{.experimental: "code_reordering".}

import nimgl/[glfw, opengl]
import streams

type
    Shader* = object
        id*: uint32


proc init_shader*(shaders: seq[tuple[`type`: GLenum, path: string]]): Shader =
    # Load, compile, and link shaders into single shader program.
    var
        ids = new_seq[uint32](shaders.len)
        sources = new_seq[cstring](shaders.len)

    for i, shader in shaders:
        # Open shader source for reading.
        var stream = new_file_stream(shader.path, fm_read)
        if is_nil(stream):
            raise new_exception(OSError, "Unable to open shader file: " & shader.path)
        defer: stream.close()

        # Read shader source.
        var contents, line: string
        while stream.read_line(line):
            contents &= line & "\n"
        sources[i] = contents

        # Compile shader.
        ids[i] = gl_create_shader(shader.`type`)
        gl_shader_source(ids[i], 1, sources[i].addr, nil)
        gl_compile_shader(ids[i])
        echo check_status(ids[i])

    # Link shaders into program.
    result.id = gl_create_program()
    for id in ids:
        gl_attach_shader(result.id, id)
    gl_link_program(result.id)
    echo result.check_status()

    # Cleanup shaders.
    for id in ids:
        gl_delete_shader(id)


proc check_status(id: uint32): string =
    ## Verify if shader compiled correctly.
    var status: int32
    gl_get_shaderiv(id, GL_COMPILE_STATUS, status.addr)
    if status != ord(GL_TRUE):
        var
            log_length: int32
            message = new_seq[char](1024)
        gl_get_shader_info_log(id, int32(len(message)), log_length.addr,
                message[0].addr)
        result = cast[string](message)


proc check_status(shader: Shader): string =
    ## Verify if program linked correctly.
    var status: int32
    gl_get_programiv(shader.id, GL_LINK_STATUS, status.addr)
    if status != ord(GL_TRUE):
        var
            log_length: int32
            message = new_seq[char](1024)
        gl_get_program_info_log(shader.id, int32(len(message)), log_length.addr,
                            message[0].addr)
        result = cast[string](message)


proc attach*(shader: Shader) =
    ## Set shader as OpenGL's active program.
    gl_use_program(shader.id)


proc delete*(shader: Shader) =
    ## Delete shader program from OpenGL.
    gl_delete_program(shader.id)


proc set_uniform*[T](shader: Shader, name: cstring, value: T) =
    ## Set value of GLSL uniform defined in shader.
    var location = gl_get_uniform_location(shader.id, name)
    case T
    of bool:
        gl_uniform1i(shader.id, location, value.int32)
    of int:
        gl_uniform1i(shader.id, location, value.int32)
    of float:
        gl_uniform1f(shader.id, location, value.float32)
    else:
        raise new_exception(TypeError, "Failed to set uniform; invalid type.")

