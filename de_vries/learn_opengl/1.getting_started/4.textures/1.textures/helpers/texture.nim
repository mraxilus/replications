{.experimental: "code_reordering".}

import nimgl/opengl
import stb_image/read as stbi

type
    Texture* = object
        id*: uint32


proc init_texture*(path: string): Texture =
    ## Initlialise 2D texture from local image file.
    var
        width, height, channels: int
        data: seq[uint8]

    # Generate and bind 2D texture.
    gl_gen_textures(1, result.id.addr)
    gl_bind_texture(GL_TEXTURE_2D, result.id)

    # Configure texture parameters.
    gl_tex_parameter_i(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT.GLint)
    gl_tex_parameter_i(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT.GLint)
    gl_tex_parameter_i(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR.GLint)
    gl_tex_parameter_i(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR.GLint)

    # Load image data into 2D texture.
    data = stbi.load(path, width, height, channels, stbi.Default)
    gl_tex_image_2d(GL_TEXTURE_2D, 0.GLint, GL_RGB.GLint, width.int32, height.int32, 0.GLint, GL_RGB,
                    GL_UNSIGNED_BYTE, data[0].addr);


proc attach*(texture: Texture) =
    ## Bind texture to current OpenGL context.
    gl_bind_texture(GL_TEXTURE_2D, texture.id)



