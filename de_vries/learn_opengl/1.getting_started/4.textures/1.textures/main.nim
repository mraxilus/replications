{.experimental: "codeReordering".}

import nimgl/[glfw, opengl]
import helpers/model


if isMainModule:
    main()


proc main() =
    # Initialise GLFW.
    assert glfw_init()
    defer: glfw_terminate()

    # Configure OpenGL.
    glfw_window_hint(GLFW_CONTEXT_VERSION_MAJOR, 3)
    glfw_window_hint(GLFW_CONTEXT_VERSION_MINOR, 3)
    glfw_window_hint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE)
    glfw_window_hint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE)

    # Create window.
    let window = glfw_create_window(800, 600, "LearnOpenGL")
    if window == nil:
        echo "Failed to create GLFW window."
        quit -1
    defer: window.destroy_window()
    window.make_context_current()

    # Setup callbacks for inputs and window resizing.
    discard window.set_key_callback(process_keys)
    discard window.set_framebuffer_size_callback(process_framebuffer_resize)

    # Initialize OpenGL.
    assert gl_init()

    # Define model data and structure.
    var
        model = init_model(
            vertices = @[
                # Rectangle.
                # Positions (3),  Colors (3), Texture Coordinates (2)
                0.5f, 0.5f, 0f, 1f, 0f, 0f, 1f, 1f, # Top right.
                0.5f, -0.5f, 0f, 0f, 1f, 0f, 1f, 0f, # Bottom right.
                -0.5f, -0.5f, 0f, 0f, 0f, 1f, 0f, 0f, # Bottom left.
                -0.5f, 0.5f, 0f, 1f, 1f, 0f, 0f, 1f, # Top left.
            ],
            indices = @[
                0'u32, 1'u32, 2'u32,  # First triangle.
                0'u32, 2'u32, 3'u32,  # Second triangle.
            ],
            attributes = @[
                (size: 3'i32, count: 4'i32),  # Positions.
                (size: 3'i32, count: 4'i32),  # Colors.
                (size: 2'i32, count: 4'i32),  # Texture Coordinates.
            ],
            shaders = @[
                (GL_VERTEX_SHADER, "shader.vert"),
                (GL_FRAGMENT_SHADER, "shader.frag"),
            ],
            texture = "texture.jpg",
        )
    defer: model.delete()

    # Perform render loop until closed.
    gl_clear_color(0.2f, 0.2f, 0.2f, 1f)
    while not window.window_should_close:
        gl_clear(GL_COLOR_BUFFER_BIT)
        model.draw()
        window.swap_buffers()
        glfw_poll_events()


proc process_keys(window: GLFWWindow, key: int32, scancode: int32,
                        action: int32, mods: int32): void {.cdecl.} =
    ## Monintor for escape key and close window if pressed.
    if key == GLFWKey.ESCAPE and action == GLFW_PRESS:
        window.set_window_should_close(true)


proc process_framebuffer_resize(window: GLFWWindow, width: int32,
                                    height: int32): void {.cdecl.} =
    ## Inform OpenGL of current window size and adjust viewport accordingly.
    gl_viewport(0, 0, width, height)

