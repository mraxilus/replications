#version 330 core

uniform sampler2D textureSampler;

in vec3 vertexColor;
in vec2 vertexTextureCoordinates;

out vec4 FragColor;

void main() {
    FragColor = texture(textureSampler, vertexTextureCoordinates) * vec4(vertexColor, 1.0);
}

