#version 330 core

layout (location = 0) in vec3 attributePosition;
layout (location = 1) in vec3 attributeColor;
layout (location = 2) in vec2 attributeTextureCoordindates;

out vec3 vertexColor;
out vec2 vertexTextureCoordinates;

void main() {
    gl_Position = vec4(attributePosition, 1.0);
    vertexColor = attributeColor;
    vertexTextureCoordinates = attributeTextureCoordindates;
}
