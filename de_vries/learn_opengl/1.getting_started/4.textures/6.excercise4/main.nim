{.experimental: "codeReordering".}

import nimgl/imgui, nimgl/imgui/[impl_glfw, impl_opengl]
import nimgl/[glfw, opengl]
import module/helpers

# Setup debug options.
var 
    clear_color: array[3, float32] = [0.2f, 0.2f, 0.2f]
    show_overlay: bool = false
    mix_amount: float32 = 0.2f

proc main() =
    # Initialise GLFW.
    do_assert glfw_init()
    defer: glfw_terminate()

    # Configure OpenGL.
    glfw_window_hint(GLFW_CONTEXT_VERSION_MAJOR, 3)
    glfw_window_hint(GLFW_CONTEXT_VERSION_MINOR, 3)
    glfw_window_hint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE)
    glfw_window_hint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE)

    # Create window.
    let window = glfw_create_window(800, 600, "LearnOpenGL")
    if window == nil:
        echo "Failed to create GLFW window."
        quit -1
    defer: window.destroy_window()
    window.make_context_current()

    # Setup callbacks for inputs and window resizing.
    discard window.set_key_callback(process_keys)
    discard window.set_framebuffer_size_callback(process_framebuffer_resize)

    # Initialize OpenGL.
    do_assert gl_init()

    # Initialize ImGui with GLFW and OpenGL implementations.
    let imgui_context = ig_create_context()
    if imgui_context == nil:
        echo "Failed to create ImGui context."
        quit -1
    defer: imgui_context.ig_destroy_context()
    do_assert ig_glfw_init_for_opengl(window, true)
    defer: ig_glfw_shutdown()
    do_assert ig_opengl3_init()
    defer: ig_opengl3_shutdown()

    # Define model data and structure.
    var
        model = init_model(
            mesh = init_mesh(
                vertices = @[
                    # Rectangle.
                    # Positions (3),  Colors (3), Texture Coordinates (2)
                    0.5f, 0.5f, 0f, 1f, 0f, 0f, 1f, 1f, # Top right.
                    0.5f, -0.5f, 0f, 0f, 1f, 0f, 1f, 0f, # Bottom right.
                    -0.5f, -0.5f, 0f, 0f, 0f, 1f, 0f, 0f, # Bottom left.
                    -0.5f, 0.5f, 0f, 1f, 1f, 0f, 0f, 1f, # Top left.
                ],
                indices = @[
                    0'u32, 1'u32, 2'u32,  # First triangle.
                    0'u32, 2'u32, 3'u32,  # Second triangle.
                ],
                attributes = @[
                    (size: 3'i32, count: 4'i32),  # Positions.
                    (size: 3'i32, count: 4'i32),  # Colors.
                    (size: 2'i32, count: 4'i32),  # Texture Coordinates.
                ],
            ),
            shader = init_shader(@[
                (GL_VERTEX_SHADER, "shaders/vertex.glsl"),
                (GL_FRAGMENT_SHADER, "shaders/fragment.glsl"),
            ]),
            textures = @[
                init_texture("textures/0.jpg"), 
                init_texture("textures/1.png"),
            ],
        )
    defer: model.delete()

    # Perform render loop until closed.
    while not window.window_should_close:
        gl_clear_color(clear_color[0], clear_color[1], clear_color[2], 1f)
        gl_clear(GL_COLOR_BUFFER_BIT)

        model.shader.set_uniform(name="mixAmount", value=mix_amount)
        model.draw()

        if show_overlay:
            ig_opengl3_new_frame()
            ig_glfw_new_frame()
            ig_new_frame()

            ig_begin("Overlay")
            ig_text("Framerate: %.1f", ig_get_io().framerate)
            ig_text("Frametime: %.1fms", 1000f/ig_get_io().framerate)
            ig_color_picker3("Clear Color", clear_color)
            ig_slider_float("Mix Amount", mix_amount.addr, 0f, 1f)
            ig_end()

            ig_render()
            ig_opengl3_render_draw_data(ig_get_draw_data())

        window.swap_buffers()
        glfw_poll_events()


proc process_keys(window: GLFWWindow, key: int32, scancode: int32,
                        action: int32, mods: int32): void {.cdecl.} =
    ## Handle keyboard interactions.
    if key == GLFWKey.UP and action == GLFW_PRESS:
        mix_amount = min(1, mix_amount + 0.1)
    elif key == GLFWKey.DOWN and action == GLFW_PRESS:
        mix_amount = max(0, mix_amount - 0.1)
    elif key == GLFWKey.SPACE and action == GLFW_PRESS:
        show_overlay = not show_overlay
    elif key == GLFWKey.ESCAPE and action == GLFW_PRESS:
        window.set_window_should_close(true)



proc process_framebuffer_resize(window: GLFWWindow, width: int32,
                                    height: int32): void {.cdecl.} =
    ## Inform OpenGL of current window size and adjust viewport accordingly.
    gl_viewport(0, 0, width, height)


if isMainModule:
    main()

