#version 330 core

uniform sampler2D textureSampler0;
uniform sampler2D textureSampler1;
uniform float mixAmount;

in vec3 vertexColor;
in vec2 vertexTextureCoordinates;

out vec4 FragColor;

void main() {
    FragColor = mix(
        texture(textureSampler0, vertexTextureCoordinates), 
        texture(textureSampler1, vertexTextureCoordinates),
        mixAmount
    );
}

