{.experimental: "code_reordering".}

import nimgl/opengl
import stb_image/read as stbi
import streams
import strformat
import strutils
import glm

include types


proc attach(mesh: Mesh) =
    # Attach all associated data to OpenGL context.
    gl_bind_vertex_array(mesh.vertex_array_object)


proc attach(model: Model) =
    ## Bind/use all associated data to current OpenGL context.
    model.mesh.attach()
    model.shader.attach()
    for i, texture in model.textures:
        model.shader.set_uniform(fmt"textureSampler{i}", i)
        texture.attach(i.uint32)


proc attach(shader: Shader) =
    ## Set shader as OpenGL's active program.
    gl_use_program(shader.id)


proc attach(texture: Texture, index: uint32 = 0) =
    ## Bind texture to current OpenGL context.
    gl_active_texture(GLenum(GL_TEXTURE0.uint32 + index))
    gl_bind_texture(GL_TEXTURE_2D, texture.id)


proc delete*(model: Model) =
    ## Delete all associated data in current OpenGL context
    model.detach()
    model.mesh.delete()
    model.shader.delete()


proc delete*(mesh: Mesh) =
    ## Delete all associated data from OpenGL context.
    gl_delete_buffers(1, mesh.vertex_buffer_object.addr)
    gl_delete_vertex_arrays(1, mesh.vertex_array_object.addr)


proc delete*(shader: Shader) =
    ## Delete shader program from OpenGL.
    gl_delete_program(shader.id)


proc detach(mesh: Mesh) =
    ## Unbind all associated data from OpenGL context.
    gl_bind_buffer(GL_ARRAY_BUFFER, 0)
    gl_bind_vertex_array(0)


proc detach(model: Model) =
    ## Unbind all associated data to current OpenGL context.
    model.mesh.detach()


proc draw*(model: Model) =
    ## Drag triangles associated with model.
    model.attach()
    gl_draw_elements(GL_TRIANGLES, model.mesh.vertices.len.int32,
            GL_UNSIGNED_INT, nil)


proc init_model*(mesh: Mesh, shader: Shader, textures: seq[Texture],
        transform: Transform = init_transform()): Model =
    ## Initialise model from encapsulation of a mesh, shader, and texture object.
    result = Model(
        mesh: mesh,
        shader: shader,
        textures: textures,
        transform: transform,
    )


proc init_model*(vertices: seq[float32], indices: seq[uint32],
                 attributes: seq[tuple[size, count: int32]],
                 shaders: seq[tuple[`type`: GLenum, path: string]],
                 textures: seq[string]): Model =
    ## Initialise model from raw data.

    var textures_init = new_seq[Texture]()
    for path in textures:
        textures_init.add(init_texture(path = path))

    result = init_model(
        mesh = init_mesh(
            vertices = vertices,
            indices = indices,
            attributes = attributes,
        ),
        shader = init_shader(
            shaders = shaders,
        ),
        textures = textures_init,
        transform = init_transform()
    )


proc init_mesh*(vertices: seq[float32], indices: seq[uint32],
        attributes: seq[tuple[size, count: int32]]): Mesh =
    ## Inititalise mesh from raw data.

    # Generate attributes.
    var attributes_init = new_seq[Attribute]()
    for a in attributes:
        attributes_init.add(init_attribute(a.size, a.count))

    result = Mesh(
        vertices: vertices,
        indices: indices,
        attributes: attributes_init,
    )

    # Generate and bind vertex array object.
    gl_gen_vertex_arrays(1, result.vertex_array_object.addr)
    gl_bind_vertex_array(result.vertex_array_object)

    # Generate and bind vertex buffer object.
    gl_gen_buffers(1, result.vertex_buffer_object.addr)
    gl_bind_buffer(GL_ARRAY_BUFFER, result.vertex_buffer_object)
    gl_buffer_data(GL_ARRAY_BUFFER, sizeof(float32) * result.vertices.len,
                   result.vertices[0].addr, GL_STATIC_DRAW)

    # Setup element buffer object from indicies.
    gl_gen_buffers(1, result.element_buffer_object.addr)
    gl_bind_buffer(GL_ELEMENT_ARRAY_BUFFER, result.element_buffer_object)
    gl_buffer_data(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32) * result.indices.len,
                   result.indices[0].addr, GL_STATIC_DRAW)

    # Determine stride for vertex data.
    var stride = 0
    for attribute in result.attributes:
        stride += attribute.size
    stride *= sizeof(float32)

    # Inform OpenGL of vertex data format.
    var offset = 0
    for i, attribute in result.attributes:
        gl_vertex_attrib_pointer(
            i.uint32,
            attribute.size,
            EGL_FLOAT,
            false,
            stride.int32,
            cast[ptr uint32](offset)
        )
        gl_enable_vertex_attrib_array(i.uint32)

        offset += attribute.size * sizeof(float32)


proc init_shader*(shaders: seq[tuple[`type`: GLenum, path: string]]): Shader =
    # Load, compile, and link shaders into single shader program.

    proc check_status(id: uint32): string =
        ## Verify if shader compiled correctly.
        var status: int32
        gl_get_shaderiv(id, GL_COMPILE_STATUS, status.addr)
        if status != ord(GL_TRUE):
            var
                log_length: int32
                message = new_seq[char](1024)
            gl_get_shader_info_log(id, int32(len(message)), log_length.addr,
                    message[0].addr)
            result = cast[string](message)

    proc check_status(shader: Shader): string =
        ## Verify if program linked correctly.
        var status: int32
        gl_get_programiv(shader.id, GL_LINK_STATUS, status.addr)
        if status != ord(GL_TRUE):
            var
                log_length: int32
                message = new_seq[char](1024)
            gl_get_program_info_log(shader.id, int32(len(message)), log_length.addr,
                                message[0].addr)
            result = cast[string](message)

    var
        ids = new_seq[uint32](shaders.len)
        sources = new_seq[cstring](shaders.len)

    for i, shader in shaders:
        # Open shader source for reading.
        var stream = new_file_stream(shader.path, fm_read)
        if is_nil(stream):
            raise new_exception(OSError, "Unable to open shader file: " & shader.path)
        defer: stream.close()

        # Read shader source.
        var contents, line: string
        while stream.read_line(line):
            contents &= line & "\n"
        sources[i] = contents

        # Compile shader.
        ids[i] = gl_create_shader(shader.`type`)
        gl_shader_source(ids[i], 1, sources[i].addr, nil)
        gl_compile_shader(ids[i])
        echo check_status(ids[i])

    # Link shaders into program.
    result.id = gl_create_program()
    for id in ids:
        gl_attach_shader(result.id, id)
    gl_link_program(result.id)
    echo result.check_status()

    # Cleanup shaders.
    for id in ids:
        gl_delete_shader(id)


proc init_texture*(
        path: string,
        wrap_mode: GLenum = GL_CLAMP_TO_EDGE,
        filter_mode: GLenum = GL_LINEAR,
    ): Texture =
    ## Initlialise 2D texture from local image file.
    var
        width, height, channels: int
        data: seq[uint8]
        path_components = path.split(".")
        extension: string = path_components[path_components.len - 1]
        color_type: GLenum

    # Generate and bind 2D texture.
    gl_gen_textures(1, result.id.addr)
    gl_bind_texture(GL_TEXTURE_2D, result.id)

    # Configure texture parameters.
    gl_tex_parameter_i(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_mode.GLint)
    gl_tex_parameter_i(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap_mode.GLint)
    gl_tex_parameter_i(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter_mode.GLint)
    gl_tex_parameter_i(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter_mode.GLint)

    # Determine if texture includes alpha value.
    case extension
    of "png":
        color_type = GL_RGBA
    of "jpg":
        color_type = GL_RGB

    # Load image data into 2D texture.
    stbi.set_flip_vertically_on_load(true)
    data = stbi.load(path, width, height, channels, stbi.Default)
    gl_tex_image_2d(GL_TEXTURE_2D, 0.GLint, GL_RGB.GLint, width.int32, height.int32, 0.GLint, color_type,
                    GL_UNSIGNED_BYTE, data[0].addr)


proc init_transform(): Transform =
    ## Generate initial transformation values repesenting the identity transformation.
    result = Transform(
        scale: vec3[float32](1f),
        rotations: vec3[float32](0f),
        translation: vec3[float32](0f),
    )


proc init_attribute*(size: int32, count: int32): Attribute =
    ## Inititalise attribute settings.
    result = Attribute(size: size, count: count)


proc rotate*(model: var Model, radians: Vec3[float32]) =
    ## Adjust rotation values of model's transformation matrix.
    model.transform.rotations += radians


proc scale*(model: var Model, scalers: Vec3[float32]) =
    ## Adjust scale values of model's transformation matrix.
    model.transform.scale += scalers


proc translate*(model: var Model, offsets: Vec3[float32]) =
    ## Adjust translation values of model's transformation matrix.
    model.transform.translation += offsets


proc calculate_transform*(model: var Model): Mat4[float32] =
    ## Generate transformation matrix from current transformation values.
    ## This is performed in the order of translation; rotation by x, y, z; and then scaling.
    result = mat4[float32](1f)
        .translate(model.transform.translation)
        .rotate(model.transform.rotations.x, vec3[float32](1f, 0f, 0f))
        .rotate(model.transform.rotations.y, vec3[float32](0f, 1f, 0f))
        .rotate(model.transform.rotations.z, vec3[float32](0f, 0f, 1f))
        .scale(model.transform.scale)


proc set_uniform*[T: bool|int|int32](shader: Shader, name: string, value: T) =
    ## Set integer or boolean value of GLSL uniform defined in shader.
    var location = gl_get_uniform_location(shader.id, name)
    gl_uniform1i(location, value.int32)


proc set_uniform*[T: float|float32](shader: Shader, name: string, value: T) =
    ## Set floating point value of GLSL uniform defined in shader.
    var location = gl_get_uniform_location(shader.id, name)
    gl_uniform1f(location, value.float32)


proc set_uniform*[T: Mat4[float32]](shader: Shader, name: string, value: T) =
    ## Set matrix value of GLSL uniform defined in shader.
    var
        location = gl_get_uniform_location(shader.id, name)
        matrix = value # Unsure if this will cause issues once this variable becomes out of scope.
    gl_uniform_matrix4fv(location, 1, false, matrix.caddr)

