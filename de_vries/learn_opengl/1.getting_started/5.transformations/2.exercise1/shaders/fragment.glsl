#version 330 core

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform float mixAmount;

in vec3 vertexColor;
in vec2 vertexTextureCoordinates;

out vec4 FragColor;

void main() {
    FragColor = mix(
        texture(texture0, vertexTextureCoordinates), 
        texture(texture1, vertexTextureCoordinates),
        mixAmount
    );
}

