{.experimental: "codeReordering".}

import math
import strformat

import glm
import nimgl/imgui, nimgl/imgui/[impl_glfw, impl_opengl]
import nimgl/[glfw, opengl]

import module/helpers

# Setup live customization options.
var
    clear_color: array[3, float32] = [0.2f, 0.2f, 0.2f]
    overlay_shown: bool = false
    texture_mix_amounts: seq[float32] = @[0.2f, 0.2f]
    scale_amounts: seq[float32] = @[1f, 1f]
    rotate_amounts: seq[float32] = @[float32(TAU / 4), 0f]
    translate_amounts: seq[array[2, float32]] = @[[0.5f, -0.5f], [-0.5f, 0.5f]]
    auto_rotate: bool = true
    auto_scale: bool = true


proc main() =
    # Initialise GLFW.
    do_assert glfw_init()
    defer: glfw_terminate()

    # Configure OpenGL.
    glfw_window_hint(GLFW_CONTEXT_VERSION_MAJOR, 3)
    glfw_window_hint(GLFW_CONTEXT_VERSION_MINOR, 3)
    glfw_window_hint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE)
    glfw_window_hint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE)

    # Create window.
    let window = glfw_create_window(800, 800, "LearnOpenGL")
    if window == nil:
        echo "Failed to create GLFW window."
        quit -1
    defer: window.destroy_window()
    window.make_context_current()

    # Setup callbacks for inputs and window resizing.
    discard window.set_key_callback(process_keys)
    discard window.set_framebuffer_size_callback(process_framebuffer_resize)

    # Initialize OpenGL.
    do_assert gl_init()

    # Initialize ImGui with GLFW and OpenGL implementations.
    let imgui_context = ig_create_context()
    if imgui_context == nil:
        echo "Failed to create ImGui context."
        quit -1
    defer: imgui_context.ig_destroy_context()
    do_assert ig_glfw_init_for_opengl(window, true)
    defer: ig_glfw_shutdown()
    do_assert ig_opengl3_init()
    defer: ig_opengl3_shutdown()

    # Define model data and structure.
    var
        vertices = @[
            # Rectangle.
            # Positions (3), Colors (3), Texture Coordinates (2)
            0.5f, 0.5f, 0f, 1f, 0f, 0f, 1f, 1f, # Top right.
            0.5f, -0.5f, 0f, 0f, 1f, 0f, 1f, 0f, # Bottom right.
            -0.5f, -0.5f, 0f, 0f, 0f, 1f, 0f, 0f, # Bottom left.
            -0.5f, 0.5f, 0f, 1f, 1f, 0f, 0f, 1f, # Top left.
        ]
        indices = @[
            0'u32, 1'u32, 2'u32, # First triangle.
            0'u32, 2'u32, 3'u32, # Second triangle.
        ]
        attributes = @[
            (size: 3'i32, count: 4'i32), # Positions.
            (size: 3'i32, count: 4'i32), # Colors.
            (size: 2'i32, count: 4'i32), # Texture Coordinates.
        ]
        shader = init_shader(@[
            (GL_VERTEX_SHADER, "shaders/vertex.glsl"),
            (GL_FRAGMENT_SHADER, "shaders/fragment.glsl"),
        ])
        textures = @[
            init_texture("textures/0.jpg"),
            init_texture("textures/1.png"),
        ]
        models = @[
            init_model(
                mesh = init_mesh(
                    vertices = vertices,
                    indices = indices,
                    attributes = attributes,
                ),
                shader = shader,
                textures = textures,
            ),
            init_model(
                mesh = init_mesh(
                    vertices = vertices,
                    indices = indices,
                    attributes = attributes,
                ),
                shader = shader,
                textures = textures,
            )
        ]
    defer: models[0].delete()
    defer: models[1].delete()

    # Perform render loop until closed.
    while not window.window_should_close:
        # Clear frame buffer with default color.
        gl_clear_color(clear_color[0], clear_color[1], clear_color[2], 1f)
        gl_clear(GL_COLOR_BUFFER_BIT)

        # Setup transformation matrix for first model.
        if auto_rotate:
            rotate_amounts[0] = ((glfw_get_time() mod TAU.float64) * 2) - TAU
        models[0].transform.scale = vec3[float32](scale_amounts[0])
        models[0].transform.rotations = vec3[float32](0f, 0f, rotate_amounts[0])
        models[0].transform.translation = vec3[float32](translate_amounts[0][0],
                translate_amounts[0][1], 0f)

        # Setup transformation matrix for second model.
        if auto_scale:
            scale_amounts[1] = sin(glfw_get_time())
        models[1].transform.scale = vec3[float32](scale_amounts[1])
        models[1].transform.rotations = vec3[float32](0f, 0f, rotate_amounts[1])
        models[1].transform.translation = vec3[float32](translate_amounts[1][0],
                translate_amounts[1][1], 0f)

        # Provide uniforms to shaders.
        for i, model in models:
            model.attach()
            model.shader.set_uniform("transform", model.calculate_transform())
            model.shader.set_uniform("mixAmount", texture_mix_amounts[i])
            model.draw()

        if overlay_shown:
            render_overlay(models)
        window.swap_buffers()
        glfw_poll_events()


proc render_overlay(models: seq[Model]) =
    ## Render console for live variable manipulation.
    ig_opengl3_new_frame()
    ig_glfw_new_frame()
    ig_new_frame()

    ig_begin("Overlay")
    ig_text("Framerate: %.1f", ig_get_io().framerate)
    ig_color_edit3("Clear Color", clear_color)
    
    for i, _ in models:
        if ig_tree_node(fmt"Model {i}"):
            ig_slider_float("Texture Mix", texture_mix_amounts[i].addr, 0f, 1f)
            ig_slider_float("Scale", scale_amounts[i].addr, 0f, 4f)
            if i == 1:
                ig_checkbox("Auto Scale", auto_scale.addr)
            ig_slider_float("Rotation", rotate_amounts[i].addr, -TAU, TAU)
            if i == 0:
                ig_checkbox("Auto Rotate", auto_rotate.addr)
            ig_slider_float2("Translate", translate_amounts[i], -2f, 2f)
            ig_tree_pop()

    ig_end()

    ig_render()
    ig_opengl3_render_draw_data(ig_get_draw_data())


proc process_keys(window: GLFWWindow, key: int32, scancode: int32,
                        action: int32, mods: int32): void {.cdecl.} =
    ## Handle keyboard interactions.
    if key == GLFWKey.SPACE and action == GLFW_PRESS:
        overlay_shown = not overlay_shown
    elif key == GLFWKey.ESCAPE and action == GLFW_PRESS:
        window.set_window_should_close(true)


proc process_framebuffer_resize(window: GLFWWindow, width: int32,
                                    height: int32): void {.cdecl.} =
    ## Inform OpenGL of current window size and adjust viewport accordingly.
    gl_viewport(0, 0, width, height)


if isMainModule:
    main()

