{.experimental: "codeReordering".}

import math
import strformat

import glm
import nimgl/imgui, nimgl/imgui/[impl_glfw, impl_opengl]
import nimgl/[glfw, opengl]

import module/helpers

# Setup live customization options.
var
    screen_width: int32 = 1280
    screen_height: int32 = 720
    clear_color = vec3[float32](0.2f, 0.2f, 0.2f)
    debug_overlay_shown = false


proc main() =
    # Initialise GLFW.
    do_assert glfw_init()
    defer: glfw_terminate()

    # Configure OpenGL.
    glfw_window_hint(GLFW_CONTEXT_VERSION_MAJOR, 3)
    glfw_window_hint(GLFW_CONTEXT_VERSION_MINOR, 3)
    glfw_window_hint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE)
    glfw_window_hint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE)

    # Create window.
    let window = glfw_create_window(screen_width, screen_height, "LearnOpenGL")
    if window == nil:
        echo "Failed to create GLFW window."
        quit -1
    defer: window.destroy_window()
    window.make_context_current()

    # Setup callbacks for inputs and window resizing.
    discard window.set_key_callback(process_keys)
    discard window.set_framebuffer_size_callback(process_framebuffer_resize)

    # Initialize OpenGL.
    do_assert gl_init()

    # Initialize ImGui with GLFW and OpenGL implementations.
    let imgui_context = ig_create_context()
    if imgui_context == nil:
        echo "Failed to create ImGui context."
        quit -1
    defer: imgui_context.ig_destroy_context()
    do_assert ig_glfw_init_for_opengl(window, true)
    defer: ig_glfw_shutdown()
    do_assert ig_opengl3_init()
    defer: ig_opengl3_shutdown()

    # Define model data and structure.
    var
        vertices = @[
            # Rectangle.
            # Positions (3), Texture Coordinates (2)
            0.5f, 0.5f, 0f, 1f, 1f, # Top right.
            0.5f, -0.5f, 0f, 1f, 0f, # Bottom right.
            -0.5f, -0.5f, 0f, 0f, 0f, # Bottom left.
            -0.5f, 0.5f, 0f, 0f, 1f, # Top left.
        ]
        indices = @[
            0'u32, 1'u32, 2'u32, # First triangle.
            0'u32, 2'u32, 3'u32, # Second triangle.
        ]
        attributes = @[
            (size: 3'i32, count: 4'i32), # Positions.
            (size: 2'i32, count: 4'i32), # Texture Coordinates.
        ]
        shader = init_shader(@[
            (GL_VERTEX_SHADER, "shaders/vertex.glsl"),
            (GL_FRAGMENT_SHADER, "shaders/fragment.glsl"),
        ])
        textures = @[
            init_texture("textures/0.jpg"),
            init_texture("textures/1.png"),
        ]
        models = @[
            init_model(
                mesh = init_mesh(
                    vertices = vertices,
                    indices = indices,
                    attributes = attributes,
                ),
                shader = shader,
                textures = textures,
                transform = init_transform(rotation = vec3[float32](-radians(55f), 0f, 0f))
            ),
        ]
        camera = init_camera(
            view = init_transform(translation = vec3[float32](0f, 0f, -3f)),
            field_of_view = TAU / 4,
            aspect_ratio = (screen_width / screen_height).float32,
            clipping_range = (near: 0.1f, far: 100f),
        )
    defer: models[0].delete()

    # Perform render loop until closed.
    while not window.window_should_close:
        # Clear frame buffer with default color.
        gl_clear_color(clear_color[0], clear_color[1], clear_color[2], 1f)
        gl_clear(GL_COLOR_BUFFER_BIT)

        camera.aspect_ratio = (screen_width / screen_height).float32
        discard camera.calculate_projection()

        # Configure and draw models.
        for i, model in models:
            model.attach()
            model.shader.set_uniform("model", model.calculate_transform())
            model.shader.set_uniform("view", camera.calculate_view())
            model.shader.set_uniform("projection", camera.calculate_projection(reuse=true))
            model.draw()

        if debug_overlay_shown:
            render_debug_overlay(camera, models)
        window.swap_buffers()
        glfw_poll_events()


proc render_debug_overlay(camera: var Camera, models: var seq[Model]) =
    ## Render console for live variable manipulation.
    ig_opengl3_new_frame()
    ig_glfw_new_frame()
    ig_new_frame()
    ig_begin("Debug Overlay")

    # Configure layout.
    ig_text("Framerate: %.1f", ig_get_io().framerate)
    ig_color_edit3("Clear Color", clear_color.arr)
    if ig_collapsing_header("Cameras"):
        camera.layout_debug_overlay("Camera 0")
    if ig_collapsing_header("Models"):
        for i, _ in models:
            models[i].layout_debug_overlay(fmt"Model {i}")

    ig_end()
    ig_render()
    ig_opengl3_render_draw_data(ig_get_draw_data())


proc process_keys(window: GLFWWindow, key: int32, scancode: int32,
                        action: int32, mods: int32): void {.cdecl.} =
    ## Handle keyboard interactions.
    if key == GLFWKey.SPACE and action == GLFW_PRESS:
        debug_overlay_shown = not debug_overlay_shown
    elif key == GLFWKey.ESCAPE and action == GLFW_PRESS:
        window.set_window_should_close(true)


proc process_framebuffer_resize(window: GLFWWindow, width: int32,
                                    height: int32): void {.cdecl.} =
    ## Inform OpenGL of current window size and adjust viewport accordingly.
    screen_width = width
    screen_height = height
    gl_viewport(0, 0, screen_width, screen_height)


if isMainModule:
    main()

