type
    Model* = object
        mesh*: Mesh
        shader*: Shader
        textures*: seq[Texture]
        transform*: Transform

    Mesh = ref object
        vertex_array_object: uint32
        vertex_buffer_object: uint32
        element_buffer_object: uint32
        vertices*: seq[float32]
        indices*: seq[uint32]
        attributes*: seq[Attribute]

    Shader = object
        id*: uint32

    Texture* = object
        id*: uint32

    Transform = object
        scale*: Vec3[float32]
        rotation*: Vec3[float32]
        translation*: Vec3[float32]

    Attribute = object
        size*: int32
        count*: int32

    Camera* = object
        field_of_view*: float32
        aspect_ratio*: float32
        clipping_range*: tuple[near: float32, far: float32]
        view*: Transform
        projection: Option[Mat4[float32]]

