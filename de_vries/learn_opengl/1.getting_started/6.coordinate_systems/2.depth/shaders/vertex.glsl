#version 330 core

layout (location = 0) in vec3 attribute_position;
layout (location = 1) in vec2 attribute_texture_coordindates;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec2 vertex_texture_coordinates;

void main() {
    gl_Position = projection * view * model * vec4(attribute_position, 1.0);
    vertex_texture_coordinates = attribute_texture_coordindates;
}
