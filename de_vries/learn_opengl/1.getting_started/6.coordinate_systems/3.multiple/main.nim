{.experimental: "codeReordering".}

import math
import strformat

import glm
import nimgl/imgui, nimgl/imgui/[impl_glfw, impl_opengl]
import nimgl/[glfw, opengl]

import module/helpers

var
    screen_width: int32 = 1280
    screen_height: int32 = 720
    clear_color = vec3[float32](0.2, 0.2, 0.2)
    debug_overlay_shown = false


proc main() =
    # Initialise GLFW.
    do_assert glfw_init()
    defer: glfw_terminate()

    # Configure OpenGL.
    glfw_window_hint(GLFW_CONTEXT_VERSION_MAJOR, 3)
    glfw_window_hint(GLFW_CONTEXT_VERSION_MINOR, 3)
    glfw_window_hint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE)
    glfw_window_hint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE)

    # Create window.
    let window = glfw_create_window(screen_width, screen_height, "LearnOpenGL")
    if window == nil:
        echo "Failed to create GLFW window."
        quit -1
    defer: window.destroy_window()
    window.make_context_current()

    # Setup callbacks for inputs and window resizing.
    discard window.set_key_callback(process_keys)
    discard window.set_framebuffer_size_callback(process_framebuffer_resize)

    # Initialize OpenGL.
    do_assert gl_init()
    gl_enable(GL_DEPTH_TEST)

    # Initialize ImGui with GLFW and OpenGL implementations.
    let imgui_context = ig_create_context()
    if imgui_context == nil:
        echo "Failed to create ImGui context."
        quit -1
    defer: imgui_context.ig_destroy_context()
    do_assert ig_glfw_init_for_opengl(window, true)
    defer: ig_glfw_shutdown()
    do_assert ig_opengl3_init()
    defer: ig_opengl3_shutdown()

    # Define model data and structure.
    let
        vertices = @[
            # Cube.
            # Positions (3), Texture Coordinates (2)
            0.5'f32, 0.5, -0.5, 1, 1, # Front top right.
            0.5, -0.5, -0.5, 1, 0, # Front bottom right.
            -0.5, -0.5, -0.5, 0, 0, # Front bottom left.
            -0.5, 0.5, -0.5, 0, 1, # Front top left.
            0.5, 0.5, 0.5, 0, 1, # Back top right.
            0.5, -0.5, 0.5, 0, 0, # Back bottom right.
            -0.5, -0.5, 0.5, 1, 0, # Back bottom left.
            -0.5, 0.5, 0.5, 1, 1, # Back top left.

            # Alternate texture coordinates (otherwise would need 24 or even 36 vertices).
            0.5, 0.5, 0.5, 1, 0, # Back top right.
            0.5, -0.5, 0.5, 1, 1, # Back bottom right.
            -0.5, -0.5, 0.5, 0, 1, # Back bottom left.
            -0.5, 0.5, 0.5, 0, 0, # Back top left.
        ]
        indices = @[
            # Traingle components of cube's faces.
            0'u32, 1, 2, # Front bottom.
            0, 2, 3, # Front top.
            0, 3, 8, # Top front (4 -> 8).
            3, 8, 11, # Top back (4 -> 8, 7 -> 11).
            0, 1, 4, # Right front.
            1, 4, 5, # Right back.
            1, 2, 9, # Bottom front (5 -> 9).
            2, 9, 10, # Bottom back (5 -> 9, 6 -> 10).
            2, 3, 6, # Left front.
            3, 6, 7, # Left back.
            4, 5, 6, # Back bottom.
            4, 6, 7, # Back top.
        ]
        attributes = @[
            (size: 3'i32, count: 4'i32), # Positions.
            (size: 2'i32, count: 4'i32), # Texture Coordinates.
        ]
        mesh = init_mesh(
            vertices = vertices,
            indices = indices,
            attributes = attributes,
        )
        shader = init_shader(@[
            (GL_VERTEX_SHADER, "shaders/vertex.glsl"),
            (GL_FRAGMENT_SHADER, "shaders/fragment.glsl"),
        ])
        textures = @[
            init_texture("textures/0.jpg"),
            init_texture("textures/1.png"),
        ]
        model = init_model(
            mesh = mesh,
            shader = shader,
            textures = textures,
        )
        positions = @[
            vec3[float32](0, 0, 0),
            vec3[float32](2, 5, -15),
            vec3[float32](-1.5, -2.2, -2.5),
            vec3[float32](-3.8, -2, -12.3),
            vec3[float32](2.4, -0.4, -3.5),
            vec3[float32](-1.7, 3, -7.5),
            vec3[float32](1.3, -2, -2.5),
            vec3[float32](1.5, 2, -2.5),
            vec3[float32](1.5, 0.2, -1.5),
            vec3[float32](-1.3, 1, -1.5),
        ]
    var
        instances = new_seq[Instance]()
        camera = init_camera(
            view = init_transform(translation = vec3[float32](0, 0, -3)),
            field_of_view = TAU / 4,
            aspect_ratio = screen_width / screen_height,
            clipping_range = (near: 0.1f, far: 100f),
        )

    # Intanciate instances with unique positons/rotations.
    for i, position in positions: 

        # Rotate model on arbitrary axis relative to world.
        let rotation = init_transform()
            .rotate(
                axis = vec3[float32](1.0, 0.3, 0.5), 
                angle = radians(20f) * i.float32,
            )
            .to_rotation()

        instances.add(init_instance(
            model = model,
            transform = init_transform(
                translation = position,
                rotation = rotation,
            ),
        ))

    # Perform render loop until closed.
    while not window.window_should_close:
        # Clear frame buffer with default color.
        gl_clear_color(clear_color[0], clear_color[1], clear_color[2], 1f)
        gl_clear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)

        # Update camera perspective projection.
        camera.aspect_ratio = (screen_width / screen_height).float32

        # Configure and draw associated models for each instance.
        for i, instance in instances:
            instance.model.attach()
            instance.model.shader.set_uniform("model", instance.transform)
            instance.model.shader.set_uniform("view", camera.view)
            instance.model.shader.set_uniform("projection", camera.to_projection())
            instance.model.draw()

        if debug_overlay_shown:
            render_debug_overlay(camera, instances)
        window.swap_buffers()
        glfw_poll_events()


proc render_debug_overlay(camera: var Camera, instances: var seq[Instance]) =
    ## Render console for live variable manipulation.
    ig_opengl3_new_frame()
    ig_glfw_new_frame()
    ig_new_frame()
    ig_begin("Debug Overlay")

    # Configure layout.
    ig_text("Framerate: %.1f", ig_get_io().framerate)
    ig_color_edit3("Clear Color", clear_color.arr)
    if ig_collapsing_header("Cameras"):
        camera.layout_debug_overlay("Camera 0")
    if ig_collapsing_header("Instances"):
        for i, _ in instances:
            instances[i].layout_debug_overlay(fmt"Instance {i}")

    ig_end()
    ig_render()
    ig_opengl3_render_draw_data(ig_get_draw_data())


proc process_keys(window: GLFWWindow, key: int32, scancode: int32,
                        action: int32, mods: int32): void {.cdecl.} =
    ## Handle keyboard interactions.
    if key == GLFWKey.SPACE and action == GLFW_PRESS:
        debug_overlay_shown = not debug_overlay_shown
    if key == GLFWKey.ESCAPE and action == GLFW_PRESS:
        window.set_window_should_close(true)


proc process_framebuffer_resize(window: GLFWWindow, width: int32,
                                    height: int32): void {.cdecl.} =
    ## Inform OpenGL of current window size and adjust viewport accordingly.
    screen_width = width
    screen_height = height
    gl_viewport(0, 0, screen_width, screen_height)


if isMainModule:
    main()

