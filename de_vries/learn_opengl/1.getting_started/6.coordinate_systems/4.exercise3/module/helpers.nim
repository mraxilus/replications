{.experimental: "code_reordering".}

import math
import options
import streams
import strformat
import strutils

import glm
import nimgl/[opengl, imgui]
import stb_image/read as stbi

include types


converter to_gluint(s: Shader): GLuint = 
    ## Allow for using shaders as arguments to OpenGL functions.
    return cast[GLuint](s.id)


converter to_gluint(t: Texture): GLuint =
    ## Allow for using textures as arguments to OpenGL functions.
    return cast[GLuint](t.id)


converter to_gluint(t: ptr Texture): ptr GLuint =
    ## Allow for using texture pointers as arguments to OpenGL functions.
    return cast[ptr GLuint](t.id.addr)


proc attach(mesh: Mesh) =
    # Attach all associated data to OpenGL context.
    gl_bind_vertex_array(mesh.vertex_array_object)


proc attach*(model: Model) =
    ## Bind/use all associated data to current OpenGL context.
    model.mesh.attach()
    model.shader.attach()
    for i, texture in model.textures:
        model.shader.set_uniform(fmt"texture{i}", i)
        texture.attach(i)


proc attach(shader: Shader) =
    ## Set shader as OpenGL's active program.
    gl_use_program(shader)


proc attach(texture: Texture, index: Natural = 0) =
    ## Bind texture to current OpenGL context.
    gl_active_texture((GL_TEXTURE0.uint32 + index.uint32).GLenum)
    gl_bind_texture(GL_TEXTURE_2D, texture)


proc layout_debug_overlay(texture: var Texture, label: string) =
    ## Generate configuration for texture object in Dear ImGUI
    if ig_tree_node(label):
        ig_text(texture.path)
        ig_tree_pop()


proc layout_debug_overlay(shader: var Shader, label: string) =
    ## Generate configuration for texture object in Dear ImGUI
    if ig_tree_node(label):
        for path in shader.paths:
            ig_text(path)
        ig_tree_pop()


proc layout_debug_overlay(transform: var Transform, label: string) =
    ## Generate configuration for modifying transform object in Dear ImGUI.
    if ig_tree_node(label):
        ig_drag_float3(
            label = "Scale",
            v = transform.scale.arr,
            v_speed = 0.01f,
        )
        ig_drag_float3(
            label = "Rotation",
            v = transform.rotation.arr,
            v_speed = TAU / 360,
            v_min = -TAU / 2,
            v_max = TAU / 2,
        )
        ig_drag_float3(
            label = "Translate",
            v = transform.translation.arr,
            v_speed = 0.01f,
        )
        ig_tree_pop()


proc layout_debug_overlay*(model: var Model, label: string) =
    ## Generate configuration for modifying model object in Dear ImGUI.
    if ig_tree_node(label):
        model.shader.layout_debug_overlay("Shader")
        if ig_tree_node("Textures"):
            for i, _ in model.textures:
                model.textures[i].layout_debug_overlay(fmt"texture{i}")
            ig_tree_pop()
        ig_tree_pop()


proc layout_debug_overlay*(instance: var Instance, label: string) =
    ## Generate configuration for modifying world instance in Dear ImGUI.
    if ig_tree_node(label):
        instance.model.layout_debug_overlay("Model")
        instance.transform.layout_debug_overlay("Model Transform")
        ig_tree_pop()



proc layout_debug_overlay*(camera: var Camera, label: string) =
    ## Generate configuration for modifying object in Dear ImGUI.
    if ig_tree_node(label):
        ig_slider_float(
            label = "Field of View",
            v = camera.field_of_view.addr,
            v_min = TAU / 360,
            v_max = TAU / 2,
        )
        ig_slider_float(
            label = "Aspect Ratio",
            v = camera.aspect_ratio.addr,
            v_min = 9 / 32,
            v_max = 32 / 9,
        )

        if ig_tree_node("Clipping Range"):
            ig_slider_float(
                label = "Near",
                v = camera.clipping_range.near.addr,
                v_min = 0f,
                v_max = camera.clipping_range.far,
            )
            ig_slider_float(
                label = "Far",
                v = camera.clipping_range.far.addr,
                v_min = camera.clipping_range.near,
                v_max = 1000f,
            )
            ig_tree_pop()

        camera.view.layout_debug_overlay("View Transform")
        ig_tree_pop()


proc delete*(mesh: Mesh) =
    ## Delete all associated data from OpenGL context.
    gl_delete_buffers(1, mesh.element_buffer_object.addr)
    gl_delete_buffers(1, mesh.vertex_buffer_object.addr)
    gl_delete_vertex_arrays(1, mesh.vertex_array_object.addr)


proc delete*(shader: Shader) =
    ## Delete shader program from OpenGL.
    gl_delete_program(shader)


proc draw*(model: Model) =
    ## Drag triangles associated with model.
    model.attach()
    gl_draw_elements(GL_TRIANGLES, model.mesh.vertices.len.int32,
            GL_UNSIGNED_INT, nil)


func init_camera*(
        view: Transform = init_transform(),
        field_of_view: float32 = TAU / 4,
        aspect_ratio: float32 = 16 / 9,
        clipping_range: tuple[near: float32, far: float32] = (0.01f, 1000f),
    ): Camera =
    ## Initialize camera with custom world to view transformation and projection.
    result = Camera(
        view: view,
        field_of_view: field_of_view,
        aspect_ratio: aspect_ratio,
        clipping_range: clipping_range,
        projection: Mat4[float32].none,
    )


func init_model*(mesh: Mesh, shader: Shader, textures: seq[Texture]): Model =
    ## Initialise model from encapsulation of a mesh, shader, and texture object.
    result = Model(
        mesh: mesh,
        shader: shader,
        textures: textures,
    )


proc init_model*(vertices: seq[float32], indices: seq[uint32],
                 attributes: seq[tuple[size, count: int32]],
                 shaders: seq[tuple[`type`: GLenum, path: string]],
                 textures: seq[string]): Model =
    ## Initialise model from raw data.
    var textures_init = new_seq[Texture]()
    for path in textures:
        textures_init.add(init_texture(path))

    result = init_model(
        mesh = init_mesh(
            vertices = vertices,
            indices = indices,
            attributes = attributes,
        ),
        shader = init_shader(
            shaders = shaders,
        ),
        textures = textures_init,
    )


proc init_instance*(model: Model, transform: Transform = init_transform()): Instance =
    ## Initialise world instance from reference model.
    result = Instance(
        model: model,
        transform: transform,
    )


proc init_mesh*(vertices: seq[float32], indices: seq[uint32],
        attributes: seq[tuple[size, count: int32]]): Mesh =
    ## Inititalise mesh from raw data.
    result = Mesh(
        vertices: vertices,
        indices: indices,
        attributes: attributes,
    )

    # Generate and bind vertex array object.
    gl_gen_vertex_arrays(1, result.vertex_array_object.addr)
    gl_bind_vertex_array(result.vertex_array_object)

    # Generate and bind vertex buffer object.
    gl_gen_buffers(1, result.vertex_buffer_object.addr)
    gl_bind_buffer(GL_ARRAY_BUFFER, result.vertex_buffer_object)
    gl_buffer_data(GL_ARRAY_BUFFER, sizeof(float32) * result.vertices.len,
                   result.vertices[0].addr, GL_STATIC_DRAW)

    # Setup element buffer object from indicies.
    gl_gen_buffers(1, result.element_buffer_object.addr)
    gl_bind_buffer(GL_ELEMENT_ARRAY_BUFFER, result.element_buffer_object)
    gl_buffer_data(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32) * result.indices.len,
                   result.indices[0].addr, GL_STATIC_DRAW)

    # Determine stride for vertex data.
    var stride = 0
    for attribute in result.attributes:
        stride += attribute.size
    stride *= sizeof(float32)

    # Inform OpenGL of vertex data format.
    var offset = 0
    for i, attribute in result.attributes:
        gl_vertex_attrib_pointer(
            index = i.uint32,
            size = attribute.size,
            `type` = EGL_FLOAT,
            normalized = false,
            stride = stride.int32,
            `pointer` = cast[ptr uint32](offset)
        )
        gl_enable_vertex_attrib_array(i.uint32)
        offset += attribute.size * sizeof(float32)


proc init_shader*(shaders: seq[tuple[`type`: GLenum, path: string]]): Shader =
    ## Load, compile, and link shaders into single shader program.

    proc check_status(id: uint32): string =
        ## Verify if shader compiled correctly.
        var status: int32
        gl_get_shaderiv(id, GL_COMPILE_STATUS, status.addr)
        if status != ord(GL_TRUE):
            var
                log_length: int32
                message = new_seq[char](1024)
            gl_get_shader_info_log(id, int32(len(message)), log_length.addr,
                    message[0].addr)
            result = cast[string](message)

    proc check_status(shader: Shader): string =
        ## Verify if program linked correctly.
        var status: int32
        gl_get_programiv(shader, GL_LINK_STATUS, status.addr)
        if status != ord(GL_TRUE):
            var
                log_length: int32
                message = new_seq[char](1024)
            gl_get_program_info_log(shader, int32(len(message)), log_length.addr,
                                message[0].addr)
            result = cast[string](message)

    var
        ids = new_seq[uint32](shaders.len)
        sources = new_seq[cstring](shaders.len)

    for i, shader in shaders:
        # Open shader source for reading.
        var stream = new_file_stream(shader.path, fm_read)
        if is_nil(stream):
            raise new_exception(OSError, "Unable to open shader file: " & shader.path)
        defer: stream.close()

        # Read shader source.
        var contents, line: string
        while stream.read_line(line):
            contents &= line & "\n"
        sources[i] = contents

        # Compile shader.
        ids[i] = gl_create_shader(shader.`type`)
        gl_shader_source(ids[i], 1, sources[i].addr, nil)
        gl_compile_shader(ids[i])
        echo check_status(ids[i])

        result.paths.add(shader.path)

    # Link shaders into program.
    result.id = gl_create_program()
    for id in ids:
        gl_attach_shader(result, id)
    gl_link_program(result)
    echo result.check_status()


    # Cleanup shaders.
    for id in ids:
        gl_delete_shader(id)


proc init_texture*(
        path: string,
        wrap_mode: GLenum = GL_CLAMP_TO_EDGE,
        filter_mode: GLenum = GL_LINEAR,
    ): Texture =
    ## Initlialise 2D texture from local image file.
    let
        path_components = path.split(".")
        extension: string = path_components[path_components.len - 1]
    var
        width, height, channels: int
        data: seq[uint8]
        color_type: GLenum

    # Generate and bind 2D texture.
    gl_gen_textures(1, result.addr)
    gl_bind_texture(GL_TEXTURE_2D, result)
    result.path = path

    # Configure texture parameters.
    gl_tex_parameter_i(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_mode.GLint)
    gl_tex_parameter_i(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap_mode.GLint)
    gl_tex_parameter_i(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter_mode.GLint)
    gl_tex_parameter_i(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter_mode.GLint)

    # Determine if texture includes alpha value.
    case extension
    of "png":
        color_type = GL_RGBA
    of "jpg":
        color_type = GL_RGB

    # Load image data into 2D texture.
    stbi.set_flip_vertically_on_load(true)
    data = stbi.load(path, width, height, channels, stbi.Default)
    gl_tex_image_2d(GL_TEXTURE_2D, 0.GLint, GL_RGB.GLint, width.int32, height.int32, 0.GLint, color_type,
                    GL_UNSIGNED_BYTE, data[0].addr)


func init_transform*(
        scale = vec3[float32](1f),
        rotation = vec3[float32](0f),
        translation = vec3[float32](0f)
    ): Transform =
    ## Generate initial transformation values repesenting the identity transformation.
    result = Transform(
        scale: scale,
        rotation: rotation,
        translation: translation,
    )



proc to_projection*(camera: var Camera, reuse: bool = false): Mat4[float32] =
    ## Caclulate projection transformation from camera attributes.
    if not reuse or camera.projection.is_none:
        camera.projection = perspective(
            camera.field_of_view,
            camera.aspect_ratio,
            camera.clipping_range.near,
            camera.clipping_range.far,
        ).some

    return camera.projection.get
    

func normalize_rotation*(transform: Transform): Transform =
    ## Ensure transform rotation angles stay in reasonable range.
    result = transform
    result.rotation = transform.rotation mod TAU


converter to_matrix*(transform: Transform): Mat4[float32] =
    ## Generate transformation matrix from current transformation values.
    ## This is performed in the order of translation; rotation by x, y, z; and then scaling.
    result = mat4[float32](1f)
        .translate(transform.translation)
        .rotate_x(transform.rotation.x)
        .rotate_y(transform.rotation.y)
        .rotate_z(transform.rotation.z)
        .scale(transform.scale)

converter to_transform*(m: Mat4[float32]): Transform =
    ## Generate transform from transformation matrix.
    result.scale = m.to_scale()
    result.translation = m.to_translation()
    result.rotation = m.to_rotation()


func rotate*(transform: Transform, axis: Vec3[float32],  angle: float32): Transform =
    ## Rotate transform by an angular amount relative to arbitrary axis using trigonometric equations.
    let rotation = mat4[float32](1)
            .rotate(angle, axis)
            .to_rotation()
    result = transform.normalize_rotation()
    result.rotation = rotation


func to_rotation*(m: Mat4[float32]): Vec3[float32] =
    ## Extract rotational component of transformation matrix along basis vectors.
    result.x = arctan2(m[2][1], m[2][2])
    result.y = arctan2(-m[2][0], sqrt(pow(m[2][1], 2) + pow(m[2][2], 2)))
    result.z = arctan2(m[1][0], m[0][0])


func to_translation*(m: Mat4[float32]): Vec3[float32] =
    ## Extract translation component of transformation matrix.
    return vec3[float32](m[0][3], m[1][3], m[2][3])


func to_scale*(m: Mat4[float32]): Vec3[float32] =
    ## Extract scalar components of transformation matrix.
    for i in 0..2:
        result[i] = sqrt(pow(m[0][i], 2) + pow(m[1][i], 2) + pow(m[2][i], 2))


proc set_uniform*[T: bool|int|int32](shader: Shader, name: string, value: T) =
    ## Set integer or boolean value of GLSL uniform defined in shader.
    let location = gl_get_uniform_location(shader, name)
    gl_uniform1i(location, value.int32)


proc set_uniform*[T: float|float32](shader: Shader, name: string, value: T) =
    ## Set floating point value of GLSL uniform defined in shader.
    let location = gl_get_uniform_location(shader, name)
    gl_uniform1f(location, value.float32)


proc set_uniform*[T: Mat4[float32]](shader: Shader, name: string, value: T) =
    ## Set matrix value of GLSL uniform defined in shader.
    let location = gl_get_uniform_location(shader.to_gluint(), name)  # Why doesn't the converter work?
    var matrix = value # Unsure if this will cause issues once this variable becomes out of scope.
    gl_uniform_matrix4fv(location, 1, false, matrix.caddr)

