#version 330 core

uniform sampler2D texture0;
uniform sampler2D texture1;

in vec2 vertex_texture_coordinates;

out vec4 FragColor;

void main() {
    FragColor = mix(
        texture(texture0, vertex_texture_coordinates), 
        texture(texture1, vertex_texture_coordinates),
        0.2
    );
}

