type
    Instance* = object
        model*: Model
        transform*: Transform

    Model* = ref object
        mesh*: Mesh
        shader*: Shader
        textures*: seq[Texture]

    Mesh = ref object
        vertex_array_object: uint32
        vertex_buffer_object: uint32
        element_buffer_object: uint32
        vertices*: seq[float32]
        indices*: seq[uint32]
        attributes*: seq[tuple[size, count: int32]]

    Shader* = object
        id: uint32
        paths*: seq[string]

    Texture* = object
        id: uint32
        path*: string
        size*: Vec3[uint32]

    Transform = object
        scale*: Vec3[float32]
        rotation*: Vec3[float32]
        translation*: Vec3[float32]

    Camera* = object
        # TODO: Modify generic camera class to be based on rotors, to avoid gimbal lock.
        field_of_view*: float32
        aspect_ratio*: float32
        clipping_range*: tuple[near, far: float32]
        transform*: Transform
        view: Option[Mat4[float32]]
        projection: Option[Mat4[float32]]

    CameraLookAt* = object
        field_of_view*: float32
        aspect_ratio*: float32
        clipping_range*: tuple[near, far: float32]
        position*: Vec3[float32]
        right*: Vec3[float32]
        up*: Vec3[float32]
        look_at: Mat4[float32]

