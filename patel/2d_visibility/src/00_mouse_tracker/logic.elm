port module Main exposing (Flags, Model, Msg(..), getPosition, init, main, mouseMove, render, subscriptions, update)

import Array exposing (Array)
import Json.Decode
import Platform exposing (worker)


main =
    Platform.worker
        { init = init
        , update = update
        , subscriptions = subscriptions
        }


port mouseMove : (Array Float -> msg) -> Sub msg


port render : Model -> Cmd msg



-- Model


type alias Flags =
    {}


type alias Model =
    { x : Float
    , y : Float
    }


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( Model 0 0, Cmd.none )



-- Update


type Msg
    = Position Float Float


update : Msg -> Model -> ( Model, Cmd Msg )
update message model =
    case message of
        Position x y ->
            let
                model_ =
                    { model | x = x, y = y }
            in
            ( model_, render model_ )


getPosition : Int -> Array Float -> Float
getPosition position array =
    case Array.get position array of
        Nothing ->
            0

        Just x ->
            x


subscriptions : Model -> Sub Msg
subscriptions model =
    mouseMove (\position -> Position (getPosition 0 position) (getPosition 1 position))
