
var ELM = Elm.Main.worker();
var CANVAS = document.getElementById("canvas");

ELM.ports.render.subscribe(function(model) {
  console.log(model);

  var context = CANVAS.getContext("2d");
  context.clearRect(0, 0, CANVAS.width, CANVAS.height);
  context.fillStyle = "#ff595d";
  context.beginPath();
  context.arc(model.x, model.y, 4, 0, 2*Math.PI, false);
  context.fill();
});

function getMousePosition(canvas, event) {
    var rectangle = canvas.getBoundingClientRect();
    return {
        x: (event.clientX - rectangle.left) / (rectangle.right - rectangle.left) * canvas.width,
        y: (event.clientY - rectangle.top) / (rectangle.bottom - rectangle.top) * canvas.height
    };
}

canvas.onmousemove = function(event) {
  position = getMousePosition(CANVAS, event);
  ELM.ports.mouseMove.send([position.x, position.y]);
};
