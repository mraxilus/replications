port module Main exposing (Flags, Line, Model, Msg(..), Point, Ray, Segment, getDistance, getIntersection, getPosition, init, main, mouseMove, render, subscriptions, toLine, toPoint, update)

import Array exposing (Array)
import Json.Decode
import Platform exposing (worker)


main =
    Platform.worker
        { init = init
        , update = update
        , subscriptions = subscriptions
        }



-- Events


port mouseMove : (Array Float -> msg) -> Sub msg


port render : Model -> Cmd msg



-- Model


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( { ray = Ray (Segment (Point 420 180) (Point 420 180)) Nothing
      , segments =
            [ Segment (Point 300 100) (Point 300 170) -- vertical.
            , Segment (Point 350 250) (Point 450 250) -- horizontal.
            , Segment (Point 450 75) (Point 550 175) -- diagonal.
            , Segment (Point 650 170) (Point 650 275) -- semi-covered.
            , Segment (Point 150 120) (Point 150 230) -- covered.
            , Segment (Point 275 80) (Point 275 270) -- eclipse.
            , Segment (Point 300 180) (Point 300 250) -- slit.
            ]
      }
    , Cmd.none
    )


type alias Flags =
    {}


type alias Model =
    { ray : Ray
    , segments : List Segment
    }


type alias Point =
    { x : Float
    , y : Float
    }


type alias Segment =
    { start : Point
    , end : Point
    }


type alias Ray =
    { segment : Segment
    , intersection : Maybe Point
    }



-- Update


subscriptions : Model -> Sub Msg
subscriptions model =
    mouseMove (\position -> Position (getPosition 0 position) (getPosition 1 position))


type Msg
    = Position Float Float


getPosition : Int -> Array Float -> Float
getPosition position array =
    case Array.get position array of
        Nothing ->
            0

        Just x ->
            x


update : Msg -> Model -> ( Model, Cmd Msg )
update message model =
    case message of
        Position x y ->
            let
                mouse_point =
                    Point x y

                mouse_line =
                    toLine model.ray.segment.start mouse_point

                segment_lines =
                    List.map (\s -> toLine s.start s.end) model.segments

                intersections =
                    List.map (\s -> getIntersection mouse_line s) segment_lines

                intersections_valid =
                    List.filterMap identity intersections

                intersection_closest =
                    List.head (List.sortBy (\( l, _ ) -> l.t) intersections_valid)

                intersection =
                    case intersection_closest of
                        Just ( l, _ ) ->
                            Just (toPoint l)

                        Nothing ->
                            Nothing

                ray =
                    Ray (Segment model.ray.segment.start mouse_point) intersection

                model_ =
                    { model | ray = ray }
            in
            ( model_, render model_ )


toLine : Point -> Point -> Line
toLine a b =
    { x = a.x, y = a.y, dx = b.x - a.x, dy = b.y - a.y, t = 1 }


type alias Line =
    { x : Float
    , y : Float
    , dx : Float
    , dy : Float
    , t : Float
    }


toPoint : Line -> Point
toPoint a =
    Point (a.x + a.t * a.dx) (a.y + a.t * a.dy)


getIntersection : Line -> Line -> Maybe ( Line, Line )
getIntersection a b =
    let
        -- Calculate the position along both lines wherin they intersect.
        at =
            (b.dy * (a.x - b.x) + b.dx * (b.y - a.y)) / (a.dy * b.dx - a.dx * b.dy)

        bt =
            (a.dy * (b.x - a.x) + a.dx * (a.y - b.y)) / (b.dy * a.dx - b.dx * a.dy)
    in
    -- Determine if the intersection is within the defined bounds of the lines.
    if at >= 0 && at <= 1 && bt >= 0 && bt <= 1 then
        Just ( { a | t = at }, { b | t = bt } )

    else
        Nothing


getDistance : Point -> Line -> Float
getDistance a b =
    let
        -- Calculate point at which a line perpendicular to 'b' will intersect with 'a'.
        b_ =
            { b | t = (b.dy * a.y + b.dx * a.x - b.dx * b.x - b.dy * b.y) / (b.dx ^ 2 + b.dy ^ 2) }

        c =
            toPoint b_
    in
    sqrt ((a.x - c.x) ^ 2 + (a.y - c.y) ^ 2)
