
var ELM = Elm.Main.init();
var CANVAS = document.getElementById("canvas");



/* Events */

canvas.onmousemove = function(event) {
  var rectangle = CANVAS.getBoundingClientRect();
  var position = {
    x: (event.clientX - rectangle.left) / (rectangle.right - rectangle.left) * CANVAS.width,
    y: (event.clientY - rectangle.top) / (rectangle.bottom - rectangle.top) * CANVAS.height
  };

  ELM.ports.mouseMove.send([position.x, position.y]);
};



/* View */

ELM.ports.render.subscribe(function(model) {
  console.log(model);

  var context = CANVAS.getContext("2d");
  //var center = {x: CANVAS.width/2, y: CANVAS.height/2};
  var ray = model.ray;
  var segments = model.segments;

  context.clearRect(0, 0, CANVAS.width, CANVAS.height);
  
  for(var i = 0; i < segments.length; i++) {
    drawLine(context, segments[i].start, segments[i].end, "#57a456", 2);
  }
  if (ray.intersection === null) {
    drawLine(context, ray.segment.start, ray.segment.end, "#ff995c", 2);
    drawPoint(context, ray.segment.end, "#ff995c");
  } else {
    drawLine(context, ray.segment.start, ray.intersection, "#ff995c", 2);
    drawLine(context, ray.intersection, ray.segment.end, "#ff595d", 1);
    drawPoint(context, ray.segment.end, "#ff595d");
    drawPoint(context, ray.intersection, "#ff595d");
  }
});


function drawLine(context, start, end, color, width) {
  context.beginPath();
  context.moveTo(start.x, start.y);
  context.lineTo(end.x, end.y);
  context.closePath();
  context.strokeStyle = color;
  context.lineWidth = width;
  context.stroke();
}


function drawPoint(context, position, color) {
  context.beginPath();
  context.moveTo(position.x, position.y);
  context.arc(position.x, position.y, 4, 0, 2*Math.PI, false);
  context.closePath();
  context.fillStyle = color;
  context.fill();
}
